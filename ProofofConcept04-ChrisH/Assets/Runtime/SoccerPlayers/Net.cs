﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Net : MonoBehaviour {
    
    [SerializeField]
    string goalTeam;
    Text displayTeam;
    [SerializeField]
    Text displayScore;
    int teamScore;
    public UnityEvent scoreGoal;
    public UnityEvent ballPoint;
    

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.GetComponent<BallController>() != null) {
            scoreGoal.Invoke();
            teamScore++;
            displayScore.text = teamScore.ToString();
            if (collision.GetComponent<BallController>().ballTeam != goalTeam && collision.GetComponent<BallController>().ballTeam != "SOLO") {
                ballPoint.Invoke();
            }
        }
    }
}
