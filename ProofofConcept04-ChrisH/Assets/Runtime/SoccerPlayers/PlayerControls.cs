﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour {

    public GameObject ball;
    public Transform net;
    public Transform enemyNet;
    [SerializeField]
    float playerSpeed;
    [SerializeField]
    string team;
    Rigidbody2D player_RB;
    
    void Start() {
        player_RB = GetComponent<Rigidbody2D>();
    }

    
    void Update() {
        if (ball.transform.position.x < transform.position.x && team == "BLUE") {
            transform.right = net.position - transform.position;
            player_RB.velocity = transform.right * playerSpeed * 3 * Time.deltaTime;
        } else if (ball.transform.position.x > transform.position.x && team == "RED") {
            transform.right = net.position - transform.position;
            player_RB.velocity = transform.right * playerSpeed * 3 * Time.deltaTime;
        } else {
            transform.right = ball.transform.position - transform.position;
            player_RB.velocity = transform.right * playerSpeed * Time.deltaTime;
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject == ball) {
            if (team == "BLUE") {
                ball.GetComponent<Rigidbody2D>().AddForce(new Vector2(150, 0));
            } else if (team == "RED") {
                ball.GetComponent<Rigidbody2D>().AddForce(new Vector2(-150, 0));
            }

            if (ball.transform.position.y < 0) {
                ball.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 75));
            } else if (ball.transform.position.y > 0) {
                ball.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -75));
            }
        }
    }
}
