﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    Rigidbody2D ball_RB;
    [SerializeField]
    private float ballSpeed;
    public string ballTeam;

    void Start() {
        ball_RB = GetComponent<Rigidbody2D>();
    }

    
    void Update() {
        #region basic movement
        if (Input.GetButton("Horizontal")) {
            ball_RB.velocity += new Vector2(ballSpeed * Time.deltaTime * Input.GetAxis("Horizontal"), 0);
        }
        if (Input.GetButton("Vertical")) {
            ball_RB.velocity += new Vector2(0, ballSpeed * Time.deltaTime * Input.GetAxis("Vertical"));
        }
        #endregion
    }
}
