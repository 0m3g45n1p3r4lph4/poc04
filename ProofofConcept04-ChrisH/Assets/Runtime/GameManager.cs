﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    UnityEvent Goal;
    bool netScored;
    [SerializeField]
    Text goalText;
    [SerializeField]
    Text ballDisplay;
    int ballScore;
    float roundTime = 20;
    [SerializeField]
    Text timerDisplay;
    float TimeRemaining;
    public Vector3[] positions;
    public GameObject[] actors;
    //positions are starting points for actors. ball = 0, 1-3 = red, 4-6 = blue
    public string[] teams;
    public Material[] teamMats;

    private void Start() {
        for (int i=0; i< actors.Length; i++) {
            positions[i] = actors[i].transform.position;
        }
        Time.timeScale = 1;
        TimeRemaining = roundTime;
        if (Goal == null) {
            Goal = new UnityEvent();
        }
        Goal.AddListener(GoalScored);
        goalText.enabled = false;
    }
    private void Update() {
        TimeRemaining -= Time.unscaledDeltaTime;
        if (TimeRemaining <= 0) {
            if (netScored == false && actors[0].GetComponent<BallController>().ballTeam == "SOLO") {
                BallScores();
            }
            goalText.enabled = false;
            ReloadScene();
        }
        timerDisplay.text = ((int)TimeRemaining).ToString();
    }

    public void ReloadScene() {
        for (int i = 0; i < actors.Length; i++) {
            actors[i].GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            actors[i].transform.position = positions[i];
        }
        actors[0].GetComponent<BallController>().ballTeam = teams[Random.Range(0, teams.Length)];
        if (actors[0].GetComponent<BallController>().ballTeam == "RED") {
            actors[0].GetComponentInChildren<MeshRenderer>().material = teamMats[1];
        } else if (actors[0].GetComponent<BallController>().ballTeam == "BLUE") {
            actors[0].GetComponentInChildren<MeshRenderer>().material = teamMats[2];
        } else if (actors[0].GetComponent<BallController>().ballTeam == "SOLO") {
            actors[0].GetComponentInChildren<MeshRenderer>().material = teamMats[0];
        }

        Time.timeScale = 1;
        TimeRemaining = roundTime;
    }

    public void GoalScored() {
        goalText.enabled = true;
        netScored = true;
        Time.timeScale = 0.25f;
        TimeRemaining = 2.5f;
    }
    
    public void BallScores() {
        ballScore++;
        ballDisplay.text = ballScore.ToString();
    }
}
